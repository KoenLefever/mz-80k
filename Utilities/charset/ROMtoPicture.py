# ROMtoPicture.py
# program to convert the Sharp character ROM to an image
# Koen Lefever 2022

import pygame

screen = pygame.display.set_mode((16*9*4+4, 16*9*4+4))
image = pygame.Surface((16*9+1, 16*9+1))
image.fill((0,50,0))
screen.blit(image, (0, 0))
pygame.display.update()

xi = 0 # x index
yi = 0 # y index
lt = 0 # line counter
br = 0 # break counter

try:
    with open('80KCG.ROM', "rb") as f:
        byte = f.read(1)
        #print(byte)
        while byte:
            ib = int.from_bytes(byte,'big')
            #print(ib)
            if ib & 0b10000000 >0:
                image.set_at((xi+1, yi+1),(255,255,255))
            else:
                image.set_at((xi+1, yi+1),(0,0,0))
            if ib & 0b01000000 >0:
                image.set_at((xi+2, yi+1),(255,255,255))
            else:
                image.set_at((xi+2, yi+1),(0,0,0))
            if ib & 0b00100000 >0:
                image.set_at((xi+3, yi+1),(255,255,255))
            else:
                image.set_at((xi+3, yi+1),(0,0,0))
            if ib & 0b00010000 >0:
                image.set_at((xi+4, yi+1),(255,255,255))
            else:
                image.set_at((xi+4, yi+1),(0,0,0))
            if ib & 0b00001000 >0:
                image.set_at((xi+5, yi+1),(255,255,255))
            else:
                image.set_at((xi+5, yi+1),(0,0,0))
            if ib & 0b00000100 >0:
                image.set_at((xi+6, yi+1),(255,255,255))
            else:
                image.set_at((xi+6, yi+1),(0,0,0))
            if ib & 0b00000010 >0:
                image.set_at((xi+7, yi+1),(255,255,255))
            else:
                image.set_at((xi+7, yi+1),(0,0,0))
            if ib & 0b00000001 >0:
                image.set_at((xi+8, yi+1),(255,255,255))
            else:
                image.set_at((xi+8, yi+1),(0,0,0))
            yi += 1
            if (br+1) % 128 == 0:
                lt += 1
                yi = 9*lt
                xi = 0
                #print(lt,yi,xi)
            elif (yi+1) % 9 == 0:
                yi = 9*lt
                xi += 9
            byte = f.read(1)
            br += 1
            #print(byte)
            
except IOError:
     print('Error While Opening the file!') 

#print (lt,yi,xi)
#image.set_at((xi+7, yi),(255,0,0)) # set red test pixel
#screen.blit(image, (0, 0))
screen.blit(pygame.transform.scale(image, screen.get_size()), (0, 0))
pygame.display.update()  # or pygame.display.flip()
#pygame.image.save(image, "MZ80Kcharset.bmp")
pygame.image.save(image, "MZ80Kcharset.png")
